#include "Int_DigitalTube.h"
#include<STC89C5xRC.H>

static u8 s_codes[10]={
	0x3F,//0
	0x06,//1
	0x5B,//2
	0x4F,//3
	0x66,//4
	0x6D,//5
	0x7D,//6
	0x07,//7
	0x7F,//8
	0x6F//9
};

u8 s_buffer[8];

/**
 * @brief 内部方法，让数管码显示某一位特定数字
 * 
 * @param position 片选。从左到右[0-7]
 * @param num_code 显示想要的数字编码
 */
static void Int_DigitalTube_DisplaySingle(u8 position, u8 num_code) 
{
	P0=0x00;//将前面的清除
	//位选P13、P14、P15
	position <<= 3;//左移三位
	P1 &= 0xC7;//P1&11000111;
	P1 |= position;
	//段选P0
	P0 = num_code;
}


void Int_DigitalTube_Init()
{
    SGM_ED = 0;
	LED_ED = 0;
}


//显示八位数，故u8不合适,使用u32以避免显示位数不够
/**
 * @brief 设置显存数组
 * 
 * @param num 待展示数字
 */
void Int_DigitalTube_DisplayNum(u32 num)
{
    //每展示一个，将这八份数组清空,否则若上一次出现的数比这一次多会被覆盖
	u8 i = 0;
	for (i; i < 8; i++)
	{
		s_buffer[i]=0x00;
	}
	
	/*buffer[7] = codes[num%10];
	num/=10;
	buffer[6] = codes[num%10];
	num/=10;
	buffer[5] = codes[num%10];
	num/=10;
	buffer[4] = codes[num%10];
	num/=10;
	bueffer[3] = codes[num%10];
	num/=10;*/
	i=7;
	if(num==0)
	{
		s_buffer[7]=s_codes[0];
		return;
	}
	/*for (i = 8; i > 0 ; i--)
	{
		buffer[i] = codes[num%10];
		num/=10;
	}
	*/
	while(num>0)
	{
		s_buffer[i] = s_codes[num%10];
		num/=10;
		i--;
	}
}

/**
 * @brief 动态扫描
 * 
 */
//动态扫描
void Int_DeigitalTUbe_Refresh()
{
    u8 i;
	for (i = 0; i < 8; i++)
	{
		Int_DigitalTube_DisplaySingle(i,s_buffer[i]);
		Com_Util_Delay1ms(1);
	}
}